# Libreyana 8: [*Unprecedented Dabbling*](http://archive.org/details/libreyana-8-2015-03-15)  

### Presented by SaltyHash  
\* Get your name here! (Check out bullet-point three in __Comments__) 

## Audio

 * [Best - FLAC; apprx. 340mb](http://archive.org/details/libreyana-8-2015-03-15/libreyana-8-2015-03-15.flac)
 * [High - Vorbis 256kbps; apprx. 110mb](http://archive.org/details/libreyana-8-2015-03-15/libreyana-8-2015-03-15-vorbis256kbps.ogg)
 * [Mid - Vorbis 112kbps; apprx. 50mb](http://archive.org/details/libreyana-8-2015-03-15/libreyana-8-2015-03-15-vorbis112kbps.ogg)  
 * [Low - Opus 56kbps; apprx. 30mb](http://archive.org/details/libreyana-8-2015-03-15/libreyana-8-2015-03-15-opus56kbps.ogg)
 * [Very Low - Opus 24kbps; apprx. 10mb](http://archive.org/details/libreyana-8-2015-03-15/libreyana-8-2015-03-15-opus24kbps.ogg)  

## Comments  

I've forgotten about all the handy dns forwarding addresses I've secured over at <http://freedns.afraid.org>.  
Let me share my favorite at the moment with ya. Instead of using <http://amateurzen.us> to get to my homepage,  
you can instead use <http://zen.neon.org>. Sweeet. By next week, I'll have secured one that contains the term  
'libre' and maybe 'radio' too, and I'll take that and run with it, 'cause let me admit one thing:  
__'amateur' was not neccessarily a wise choice of terms to include in my base url.__  
For mass-appeal, it's just slightly too tricky to spell.   
 
I've settled on a couple of new principles by which this establishment will live:  

 * 00:02:00 minute breaks @ 00:15:00 minute increments, approximately (See __Breaks__ section below). Call it a "bathroom song", if you will, OR:  
 Put your station ID or your bogative underwriting/advertisement over it. We call those "liberties". You can take 'em.  
 Of course, I'd love for you to listen to me credit the tracks and rant & ramble.  
   
 * I have so little respect for reigning financial services industry that I would like to cut as many of their tentacles as possible out of the dealings of *amateurzen.us*, *Libreyana*, and any other future online projects of mine. I'm currently trying to iron out details of how I can best take advantage of all of the new tools being built upon the blockchain core of Satoshi's *[bitcoin](https://www.dmoz.org/Science/Social_Sciences/Economics/Financial_Economics/Currency_and_Money/Alternative_Monetary_Systems/Bitcoin)* (BTC). Eventually, I'll probably offer an official not-for-profit pledge. It is very unlikely that I'll ever ask for any official decree of status from the government of the great-state of the USG. I believe that the nature of the blockchain makes my activity inherently transparent. At the moment, there are two components that need worked out: What is the best service I can offer to those who want to use a major debit/credit card or a major online money-changer to send currency my way so that it is already converted to BTC by the time I receive it. I *think* [Coinbase](https://www.coinbase.com/) may provide some service like this. Secondly, and double-edged, how to get those artists and labels I feature that don't already own a BTC (or exchangeable, alternative digital currency) wallet to GET a wallet so that I can begin sending those artists a proportion of the donations I receive for each show. At this point, I'm close to sure I can send any Tweeder user a BTC tip via [@Changetip](http://changetip.com). This is probably the best solution for now. This idea is getting intertwingled with the idea of a "label" of a sort which utilizes a multi-signature lockbox for operational expenses. Apparently, I'm trying to build a label without (neccessarily) prior consent of the artists whose free cultural work the label is built upon. Once I get some actual rules laid down, the transparency of the blockchain should act as an unprecedented shield against dishonest accounting. Okay - that's probably enough rambling text for one week. Talk to me at ao AT amateurzen.us if your interested in any of this or if you have any approximate expertise to offer.  
 
 * Since I'm going on about finances, let me spend a moment thanking [Zeronode](http://irc.zeronode.net/) IRC user SaltyHash for his generous [Litecoin](https://litecoin.org/) (LTC) payments for nearly every episode he's consumed of the *Amateur Zen 'Cast* and its recent reiteration known as *Libreyana*. I like to steal the best-practices of the [No Agenda Show](http://noagendashow.com) where and when I can, so SaltyHash retains the credit as Executive Producer for yet another week, and this week I'll start a tradition of adding the "Presented by..." line for the Executive Producer. If there were another contributor(s) over the course of the week who'd contributed more than SaltyHash, they'd have received the credit. Any contributor(s) donating less will then be known by a more general term: Venture Capitalist. Know that I'll be dwelling on the more holistic definition of the term *capital*, and the beautiful diversity, and the fact that *venture* = *adventure* - ad. Now that's something.  
 
 * OK, lastly, I'm thinking of starting up a [Github](https://github.com/) repository (or an alternative-to) with the purpose of storing these shownotes. One of my goals for this program is to fight against the dead-link tide and keep its container(s) intact and integrous for the long-term. Keeping these documents in a publicly-visible and editable location seems like a feature :)  
 Maybe some of you'd like to help edit shownotes. Maybe some of the artists I'd feature would like an easy path to edit the data corresponding to their work. Maybe I should figure out how to self-host it, maybe not. I know that there's something better than Google's docs out there waiting for me to discover and/or use it. Wish me luck, and as usual, advice welcome.  
   
   
__Links__:  

 * The [Free Academy](http://freeacad.com)'s Art Contest:  
   <http://freeacad.com/wp/freer-people-art-contest/>  
   All Rights Reserved  
   
 * *[Rendering Unto Caesar](http://fee.org/publications/detail/rendering-unto-caesar-was-jesus-a-socialist):*  
   *Was Jesus a Socialist?*  
   by [Lawrence W. Reed](http://fee.org/authors/detail/lawrence-w-reed). Published under the [Creative Commons™ Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0)  
   by [FEE](http://fee.org) (The Foundation for Economic Education)  
   Direct [Link](http://fee.org/files/doclib/20150305_ReedRenderingUntoCaesar.pdf) to PDF  
   
      
   
__New artists this week__:  

 * [The Always Altered Collaboration](https://archive.org/details/alwaysaltered) from Phoenix, Arizona ft. [Mr. Jardin](https://soundcloud.com/mr-jardin-trip-hop) from Toulouse, France. Also [on Youtube](https://www.youtube.com/channel/UC3sZFsM7q1LZj9NrrtCJW3Q).  
 * [The Copycuts](http://copycuts.com/) from Gold Coast, Australia. Also [on Soundcloud](https://soundcloud.com/copycuts).  
 * [Elemint B. Fresh](http://elemintbornfresh.com/) from Indianapolis, Indiana. Also [on Facebook](https://www.facebook.com/elemintbornfresh/timeline) and [on Soundcloud](https://soundcloud.com/elemint-b-fresh).
 * [JANP](https://www.jamendo.com/en/artist/462262/janp) from Uruguay. Also [on Facebook](https://www.facebook.com/JANProductionz).    
 * [WALKIE TALKIE](http://soundcloud.com/wlkietlkie) from San Diego, California. Also [on Facebook](https://www.facebook.com/pages/WALKIE-TALKIE/253222334735152?ref=hl) and [on Twitter](https://twitter.com/walkiebeats).    
 * Z1 Nation, Pekanzo, & [Wu Lords](http://www.wulords.com/) (FLASH required. Boo.) from Pittsburgh, US. Also [on Facebook](https://www.facebook.com/WuLords) and [on Twitter](http://twitter.com/wulords/).    
 
 
__Breaks for Speech__:  

 * 00:10:00 - 00:38:00 >> Hello/Welcome 
 * 00:15:16 - 00:17:24 >> Track Credits  
 * 00:30:00 - 00:33:34 >> Track Credits & talking about the Free Academy's "Freer People" art project.
 * 00:45:15 - 00:52:58 >> Track Credits & a reading from *Rendering Unto Caesar*  
 * 00:55:20 - 00:56:42 >> Track Credits & Farewell 
 
 
__Shownotes Credits Format__:  

"Title" (If a single, links to release page. Otherwise, no link here.)  
from *Album* (Links to Album release page.)  
by Artist (Links to Artist's homepage, or most appropriate url the Artist self-associates with.)  
License (Links to human-readable summary of the public license to use the work.)  

*If the "album" is a compilation of a sort, then usually the "by Artist" comes before the "from Album" line.   

When I get around to archiving all of these releases in The Internet Archive's Wayback Machine,  
I'd like to present a list of those archived urls in a separate section at the bottom of the track list.  
Just another thang I'm workin' on.
 
   
## Musical Selections [^1]  

>   *Intro*  
>  An excerpt of "Apex Chappelle"  
>  from *[Route 9](Uhttp://freemusicarchive.org/music/Uncle_Milk/Route_9)*  
>  by [Uncle Milk](https://soundcloud.com/unclemilk)  
>  [CC™ BY 4.0](http://creativecommons.org/licenses/by/4.0)  

1. "Lagoon"  
   by [WALKIE TALKIE](http://soundcloud.com/wlkietlkie)        
   from [Stratford Court](http://stratfordct.bandcamp.com)'s *[1 yr Anniversary Compilation](http://stratfordct.bandcamp.com/album/1-yr-anniversary-compilation)*  
   [CC™ BY 3.0](http://creativecommons.org/licenses/by/3.0)     
 
2. "Mad Mountain Radio"  
   by [Mr. Jardin](https://soundcloud.com/mr-jardin-trip-hop)   
   from *[The Always Altered Collaboration: Volume 1](https://archive.org/details/TheAlwaysAlteredCollabVol.1)*  
   CC™ BY 3.0    

3. "Lovers Like Neon"   
   from *[Sketches](http://freemusicarchive.org/music/The_Copycuts/Sketches_1599/14_Lovers_Like_Neon)*  
   by [The Copycuts](http://copycuts.com/)  
   [CC™ BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0)     
   
4. "Parliament" & "Sky Guide"        
   from *[Little Histories](http://cloudkickermusic.com/album/little-histories)*  
   by [Cloudkicker](http://cloudkickermusic.com/)  
   CC™ BY 3.0  
   ...followed by a short excerpt from The *[No Agenda Show]* [Episode 699](http://699.noagendanotes.com)  
   by [John C. Dvorak](http://dvorak.org) aka [@TheRealDvorak](http://twitter.com/therealdvorak) & [Adam Curry](http://curry.com) aka [@adamcurry](http://twitter.com/adamcurry)    
   [CC™ BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0)  
 
5. "Lucid Nightmares"    
   from *[Unleashed 9](https://www.jamendo.com/en/list/a144153/unleashed-9)*    
   by [JANP](https://www.jamendo.com/en/artist/462262/janp)    
   CC™ BY 3.0   
   
6. "[Fallen Soldier](https://www.jamendo.com/en/track/1203252/fallen-soldier)"    
   by *[WuLords](http://www.wulords.com/)*    
   CC™ BY 3.0   
   
8. "Sometimes Forward"  
   from *[Revised & Corrected: Volume 4](https://www.jamendo.com/en/list/a129852/revised-corrected-vol.4)*  
   by [Angus](http://www.webalice.it/angelo.alabiso/) aka [@angelo_alabiso](http://twitter.com/angelo_alabiso)  
   CC™ BY-SA 3.0    
   
9. "Guerrilla Music"    
   by [Elemint](http://elemintbornfresh.com/)    
   from *[FrostWire Creative Commons Mixtape Volume 2 Side A](http://freemusicarchive.org/music/FrostClick/FrostWire_Creative_Commons_Mixtape_2_Side_A)*      
   CC™ BY-SA 3.0   
   
10. "Add And"  
    from *[Petal](https://brokeforfree.bandcamp.com/album/petal)*  
    by [Broke For Free](https://www.facebook.com/brokeforfree). Also [on Soundcloud](http://soundcloud.com/broke-for-free).  
    CC™ BY 3.0     
   
10. "Lucid Dreaming"  
    from *[Chameleon](https://www.jamendo.com/en/list/a70693/chameleon)*  
    by [Capashen](http://soundcloud.com/capashen) aka [@capashen2](http://twitter.com/capashen2). Also [on Soundcloud](http://soundcloud.com/capashen).   
    [CC™ BY-SA 2.0 FR](http://creativecommons.org/licenses/by-sa/2.0/fr)      

10. "Waltz for Debby"  
    from *[FLCC](http://indiefolkjazz.bandcamp.com/album/flcc)*  
    by Friends [^2]    
    CC™ BY 3.0   
   
10. "Blinking and Breathing"  
    from *[Blinking and Breathing](https://www.jamendo.com/en/list/a143838/blinking-and-breathing)*  
    by [Simon van Gend](https://www.jamendo.com/en/artist/461591/simon-van-gend)  
    CC™ BY 3.0   
    
__Archived for safekeeping__:  

 * *Route 9* by Uncle Milk:  
   <https://web.archive.org/web/20150215212300/http://freemusicarchive.org/music/Uncle_Milk/Route_9>   
   
 
## Details  

*Libreyana* is produced in the State of Jefferson,  
and lives on the web at <http://amateurzen.us>.  

 * RSS: <http://feeds.feedburner.com/libreyana/>
 * Email: ao AT amateurzen DOT us *OR* use [a contact form]
 * PGP:  [Link](http://amateurzen.us/about) to my "About" page   
 * Mail: 1692 Mangrove Ave. #280, Chico, CA 95926
 * BTC:  13bn4xscMPS2iyC7uXtYDMTsqXiDgwQvsh
 * LTC:  LW43j52oeFWUYMupn2Nimv5NvDYiRHqaiV 
 * Doge: DLTfPnojoVkLk3hJcJfQiqU6eVfnuNQ85J

__Airtimes__:  
Sundays at 6 PM PST on __[RynoTheBearded]__'s *[#OO Stream]*.   
and Thursdays at 10 PM PST on *[KFUG Community Radio]*.   

__Legal__:  
The music you hear is free to copy, modify, and distribute. Tracks are individually licensed and hyper-links are provided to human-readable summaries of those licenses. If you do use this work, the artists ask that you mention (credit) them by name ([CC™ BY]). Most of them also ask that you Share Alike ([CC™ BY-SA]). These conditions (which are very easy to satisfy) __can be waived__ by the artist(s) aka author(s) aka creator(s) aka owner(s) of the work for any purpose. Contact the respective creator(s) if you'd like to use their work, but need further and/or more specific permissions. Most of 'em answer their own email.

I rant, ramble, read texts, credit tracks, and arrange these musical selections into a package containing notes and (sometimes) cover art; that work is ©2015 Alex O'Brien, but (hopefully) effectively dedicated to the public domain under the terms of [CC™ Zero]. I do appreciate attribution (to [Alex O'Brien] and/or [Amateur Zen]).   

__Acknowledgements__:  
Most of the musical selections were originally found at  
[Jamendo](http://jamendo.com), [Bandcamp](http://bandcamp.com), and [The Internet Archive](http://archive.org).  
I also frequent [FMA](http://freemusicarchive.org), [Starfrosch.ch](http://starfrosch.ch)...

Some of the clips of the [*No Agenda Show*] that you hear here  
may be available at <https://archive.org/details/lrhq-jingles>.    

Audio codecs (Ogg, Opus, FLAC) provided by the __Xiph.org Foundation__: <http://xiph.org/>  
Audio editing performed in __Audacity__: <http://audacity.sourceforge.net/>  
Notes iterated by __Pandoc__: <http://johnmacfarlane.net/pandoc/>  
[*dir2ogg*](http://jak-linux.org/projects/dir2ogg/), *ls*, [*opus-tools*](http://www.opus-codec.org), and *cd* are also integral to the production of this program :)


<!-- FOOTNOTES -->  

[^1]: Most selections are slightly altered, modified, and/or mixed with other selections. Many tracks are voiced over. My intent is to compose in the spirit of my inspirations: the diverse variety of terrestrial and Internet radio programming I grew up listening to and continue to consume (more "podcasts" these days, thank heavens). There are certain disc-jockey and pre-produced radio "treatments" we, as a society, have grown accustomed to hearing. These include cross-fades, fade-ins, fade-outs, auto-ducks (as Audacity and I call them), voice-overs, stingers and bumpers that may bleed slightly into primary content, etc. The last thing I intend to do is obscure the authorship of the skilled industrialists whose work I feature, or infringe upon their due credit. If you need details on any of these modifications, feel free to ask me at ao@amateurzen.us.  

[^2]: From *[indiefolkjazz](http://indiefolkjazz.bandcamp.com)*.


<!-- REFERENCE URLS --> 

[The Dada Weatherman]: http://www.thedadaweatherman.com/  
[folK.it.Up]: https://www.jamendo.com/en/list/a143735/folk.itup

[*12 O'Clock* by __Mr. Juan__]: https://www.jamendo.com/en/list/a117681/12-o-clock
[*1972 Speech* by __Buckminster Fuller__]: https://archive.org/details/BuckminsterFuller1972Speech
[#2 Orchestra]: http://www.myspace.com/2orchestra
[#2 Orchestra ft. Darkened Escape]: http://www.myspace.com/2orchestra
[*A Bed of Fallen Autumn Leaves* by __Thoola__]: http://www.jamendo.com/album/117439/
[*A Bomb Shelter in Kansas* by __The Fucked Up Beat__]: https://archive.org/details/TheFuckedUpBeatABombShelterInKansas01FearInTheDustBowl
[Adam Curry]: http://curry.com/
[*Adaption Disorder* by __Terrorrythmus__]: http://yarnaudio.bandcamp.com/album/adaption-disorder
[*After Work*]: http://www.jamendo.com/album/66506/
[After Work]: http://www.jamendo.com/en/artist/AFTER_WORK
[*A lil Somethin' Somethin'* by __The Good Lawdz__]: https://www.jamendo.com/en/list/a111378/a-lil-somethin-somethin
[Al Lover]: http://allover.bandcamp.com/
[*Am I Young? Am I Old?* by __MrJuan__]: http://www.jamendo.com/album/129366/
[an eponymous 2010 release by __After Work__]: http://www.jamendo.com/album/66506/
[an eponymous 2014 release by __Thoola__]: https://www.jamendo.com/en/list/a134661/thoola
[an eponymous 2015 release by __Tlushch__]: https://www.jamendo.com/en/list/a142886/tlushch
[Angus]: https://www.jamendo.com/en/artist/341346/angus  
[Antoine Guiry]: http://www.antoine-guiry.fr/
[Apache Tomcat]: https://archive.org/search.php?query=creator%3A%22Alex%20Ramirez%20and%20Friends%22
[*Apologetic Destruction*]: http://indiefolkjazz.bandcamp.com/album/apologetic-destruction
[*Arkaik Excavations Vol. 1* (Archived)]: https://web.archive.org/web/20150215204300/https://swahili.bandcamp.com/album/arkaik-excavations-vol-1
[Arkaik Excavations Vol. 1]: https://swahili.bandcamp.com/album/arkaik-excavations-vol-1
[*Arrythmia* (EP)]: http://thefuckedupbeat.bandcamp.com/album/arrythmia
[*Autumn Song* by __Roy Smiles__]: http://roysmiles.bandcamp.com/album/autumn-song
[\@backnbloom]: https://twitter.com/backnbloom
[BacknBloom]: http://www.jamendo.com/en/artist/Backnbloom
[*Bazar* (Archived)]: https://web.archive.org/web/20150215222746/https://lorenzosmusic.bandcamp.com/album/bazar
[*Bazar* by __Lorenzo's Music__]: http://lorenzosmusic.bandcamp.com/album/bazar
[Bazar]: http://lorenzosmusic.bandcamp.com/album/bazar
[*Beacons* (Archived)]: https://web.archive.org/web/20150215183845/http://cloudkickermusic.com/album/beacons
[*Beacons*]: http://cloudkickermusic.com/album/beacons
[Beacons]: http://cloudkickermusic.com/album/beacons
[*Being In Oblivion -　Music Called NAKAGAWA: Part 1* by __Ambient Samurai__]: http://www.jamendo.com/album/140125/
[*Boarding Pass EP* (Archived)]: https://web.archive.org/web/20150215205450/https://shadk.bandcamp.com/album/boarding-pass-ep
[Boarding Pass EP]: https://shadk.bandcamp.com/album/boarding-pass-ep
[Brad Sucks]: http://www.bradsucks.net/
[\@bradsucks]: http://www.twitter.com/bradsucks/
[*Bremen* by __Roy Smiles__]: http://roysmiles.bandcamp.com/track/bremen
[Broke for Free]: https://www.facebook.com/brokeforfree
[Bryan Mathys]: https://www.jamendo.com/en/artist/459808/bryan-mathys
[*Burning Bright* EP by __Burning Bright__]: http://blacklanternmusic.bandcamp.com/album/burning-bright-ep
[*Calm* by __Tymphony__]: http://www.jamendo.com/album/45082/
[Capashen]: http://www.jamendo.com/en/artist/Capashen
[*Chameleon*]: http://www.jamendo.com/album/70693/  
[*Circa Vitae EP* (Archived)]: https://web.archive.org/web/20150215185532/https://www.jamendo.com/en/list/a35849/circa-vitae-ep
[Circa Vitae EP]: https://www.jamendo.com/en/list/a35849/circa-vitae-ep
[Circa Vitae]: http://circavitae.com/
[*City Beat* by __Ciampa__]: http://www.jamendo.com/album/111697/
[*Cleta Squad* EP by __Isaac Galvez__]: http://stratfordct.bandcamp.com/album/isaac-galvez-cleta-squad-ep/
[Cloudkicker]: http://cloudkickermusic.com/
[*Conscience is a Gift* by __Mammal is a Mountain__]: http://indiefolkjazz.bandcamp.com/album/conscience-is-a-gift
[\@coolallover]: https://twitter.com/coolallover
[Crystal Drop]: https://soundcloud.com/crystaldrop
[*Crystal Powered Engines 2* (Archived)]: https://web.archive.org/web/20150130075321/https://www.jamendo.com/en/list/a139769/crystal-powered-engines  
[Crystal Powered Engines 2]: https://www.jamendo.com/en/list/a139769/crystal-powered-engines
[*Curses* (Archived)]: https://web.archive.org/web/20150206015347/https://www.jamendo.com/en/list/a139106/curses
[Curses]: https://www.jamendo.com/en/list/a139106/curses  
[Dancing Crow]: http://dansingcrow.devhub.com/music/  
[*Defeat Lap: Arguably the Best of S.A. Bach, 2007-13* by __S.A. Bach__]: http://sabach.bandcamp.com/album/defeat-lap-arguably-the-best-of-s-a-bach-2007-13
[Defeat Lap: Arguably the Best of S.A. Bach, 2007-13]: http://sabach.bandcamp.com/album/defeat-lap-arguably-the-best-of-s-a-bach-2007-13
[Desde el Fuego]: http://archive.org/details/Hunno
[*Deus Ti Salvet Maria* (Archived)]: https://web.archive.org/web/20150130073814/https://www.jamendo.com/en/list/a142899/deus-ti-salvet-maria
[Deus Ti Salvet Maria]: https://www.jamendo.com/en/list/a142899/deus-ti-salvet-maria
[Devoured by the Comfort Zone]: https://www.jamendo.com/en/list/a140112/devoured-by-the-comfort-zone
[DJ Habett]: http://habett.net/
[*Dressing Room*]: http://www.jamendo.com/album/131729/
[*Driftwood* (Archived)]: https://web.archive.org/web/20150215184716/https://www.jamendo.com/en/list/a139327/driftwood
[Driftwood]: https://www.jamendo.com/en/list/a139327/driftwood
[*Electric Dream*]: https://www.jamendo.com/en/list/a140243/electric­dream  
[Elizabeth Barrett Browning]: https://en.wikipedia.org/wiki/Elizabeth_Barrett_Browning
[Eman X Vlooper]: http://alaclair.com/
[*Episode 670*]: http://670.noagendanotes.com
[*Episode 674*]: http://674.noagendanotes.com
[*Episode 682*]: http://682.noagendanotes.com
[Esther Chlorine]: http://www.ofplants.com/  
[Evergreen Machine]: http://www.evergreenmachineband.com/  
[\@evergreenmachin]: https://twitter.com/evergreenmachin
[*Exile in Blue* by __Roy Smiles__]: https://www.jamendo.com/en/list/a128591/exile­in­blue­best­of­ii
[*Explorations Sonores* (Archived)]: https://web.archive.org/web/20150206020554/https://www.jamendo.com/en/list/a139065/explorations-sonores
[Explorations Sonores]: https://www.jamendo.com/en/list/a139065/explorations-sonores
[*Explorations Sonores*]: https://www.jamendo.com/en/list/a139065/explorations-sonores
[*Eye of the Storm*]: http://www.jamendo.com/album/140048/
[*Fade*]: http://cloudkickermusic.com/album/fade
[*First & Last* by __Funky Stereo__]: http://www.jamendo.com/album/140404/
[fitzzgerald]: https://soundcloud.com/fitzzgerald
[Floating Isle]: http://floatingisle.wordpress.com/
[*Folk and Acoustic 3*]: http://www.jamendo.com/album/140322/
[*From Sinners to Sinners* by __For the Broken__]: http://www.jamendo.com/album/139563/
[*Frostwire CC™ Mixtape Vol. 4*]: http://freemusicarchive.org/music/FrostClick/FrostWire_Creative_Commons_Mixtape_Volume_4
[*Gaia*]: http://www.jamendo.com/album/115827/
[*Girlfriend* (Archived)]: https://web.archive.org/web/20150130074949/https://stratfordct.bandcamp.com/album/fitzzgerald-girlfriend
[Girlfriend]: https://stratfordct.bandcamp.com/album/fitzzgerald-girlfriend
[*Goat Remixes*]: http://allover.bandcamp.com/album/goat-remixes  
[*Grain of Sand* by __Ground & Leaves__]: http://www.jamendo.com/album/108272/
[*Guess Who's a Mess* (Archived)]: https://web.archive.org/web/20150130080628/http://www.bradsucks.net/albums/guess-whos-a-mess/
[Guess Who's a Mess]: http://www.bradsucks.net/albums/guess-whos-a-mess/
[*Hear Come the Drums* by __Reole__]: https://www.jamendo.com/en/list/a142018/hear-come-the-drums
[Helen Lovejoy]: http://simpsons.wikia.com/wiki/Helen_Lovejoy
[Henry Wadsworth Longfellow]: https://en.wikipedia.org/wiki/Henry_Wadsworth_Longfellow
[Hey Exit]: http://www.heyexit.com/  
[*Highmas 5* by __Highmas__]: http://www.jamendo.com/album/131481/
[Hiroumi]: http://www.hiroumi.net/
[\@hiroumikun]: https://twitter.com/hiroumikun
[Homefront]: https://www.jamendo.com/en/list/a141952/homefront
[How Now Lau & DJ Murtle MC]: https://archive.org/search.php?query=creator%3A%22How+Now+Lau+%26+DJ+Murtle+MC%22  
[*Ice* by __Tymphony__]: http://www.jamendo.com/album/45019/
[Iltarusko]: https://www.jamendo.com/en/artist/444298/iltarusko
[*Interstate Medicine* by __Slim__]: http://www.jamendo.comalbum/3100/
[\@jeffnelsonmusic]: https://twitter.com/jeffnelsonmusic/
[Jeffrey Philip Nelson]: http://www.jeffreyphilipnelson.com/
[Joe Biden]: http://www.whitehouse.gov/administration/vice-president-biden
[John C. Dvorak]: http://dvorak.org  
[\@joshwoodward]: https://twitter.com/joshwoodward
[Josh Woodward]: http://www.joshwoodward.com/
[*Just Had to Let You Know* by __Lorenzo's Music__]: http://lorenzosmusic.bandcamp.com/album/just­had­to­let­you­know
[Kathleen E. Kennedy]: http://punctumbooks.com/category/titles/kathleen-e-kennedy/
[KenLo Craqnuques]: http://www.kenlocraqnuques.com/
[Kevin MacLeod]: http://incompetech.com
[__Kevin MacLeod__'s *Classical Sampler*]: http://freemusicarchive.org/music/Kevin_MacLeod/Classical_Sampler
[__Kevin MacLeod__'s *Funk and Blues* Collection]: http://incompetech.com/music/royalty-free/index.html?collection=011
[__Kevin MacLeod__'s *Jazz Sampler*]: http://freemusicarchive.org/music/Kevin_MacLeod/Jazz_Sampler/Off_to_Osaka
[*Kokoro no Furyoku* (Archived)]: https://web.archive.org/web/20150215223801/https://www.jamendo.com/en/list/a44756/kokoro-no-furyoku
[Kokoro no Furyoku]: https://www.jamendo.com/en/list/a44756/kokoro-no-furyoku
[*Kontaminazioni* by __Donato Giordono__]: http://www.jamendo.com/album/135764/
[\@laurentleemans]: https://twitter.com/laurentleemans
[*Le Migre #3: Remixes* by __Small Radio__ & Various Artists]: http://rec72.net/?p=3601
[*Little Histories* by __Cloudkicker__]: http://cloudkickermusic.com/album/little-histories
[*LM3 RMX* by __Small Radio__ & Various Artists]: http://rec72.net/?p=3601
[Löhstana David]: http://www.jamendo.com/en/artist/david
[*Lone Guitar Player* by __Andrey Avkhimovich__]: http://www.jamendo.com/album/139680/
[*Look Through the Microscope* by __Pleiades M__]: http://rec72.net/?p=3584
[*Loop* by __Cloudkicker__]: http://cloudkickermusic.com/album/loop
[\@lorenzosmusic]: http://twitter.com/lorenzosmusic
[__Lorenzo's Music__]: http://www.lorenzosmusic.com/
[Lorenzo's Music]: http://www.lorenzosmusic.com/
[*Lovers* EP by __Circa Vitae__]: http://www.jamendo.com/album/119336/
[LRockHQ]: http://twitter.com/lrockhq
[Luzius Stone]: http://www.jamendo.com/en/artist/Luzius_Stone
[*Lyzardry* (Archived)]: https://web.archive.org/web/20150206021748/https://www.jamendo.com/en/list/a142013/lyzardry
[Lyzardry]: https://www.jamendo.com/en/list/a142013/lyzardry
[Marc Reeves]: http://www.marcreeves.co.uk/
[\@marcreevesmusic]: https://twitter.com/marcreevesmusic
[Medieval Hackers]: http://punctumbooks.com/titles/medieval-hackers/
[*Method Act* by __Method Act__]: http://methodact1.bandcamp.com/album/method­act
[__Mike Allen__]: http://www.jamendo.com/en/artist/437571
[Mike Allen]: http://www.jamendo.com/en/artist/437571
[__Mike Schpitz__]: http://www.mikeschpitz.com
[Missing Linkz]: https://archive.org/details/HNLDJM04  
[Modern Pitch]: http://www.jamendo.com/en/artist/Modern_Pitch
[Modulo Negus]: https://www.jamendo.com/en/list/a138646/modulo-negus
[*Muddle in Your Bones* by __Thrifter__]: http://indiefolkjazz.bandcamp.com/album/muddle-in-your-bones
[*Musical Roads* by __BFJazz__]: https://www.jamendo.com/en/list/a142005/musical-roads
[*Music for the Modern Monkey* (Archived)]: https://web.archive.org/web/20150130072521/https://www.jamendo.com/en/list/a126867/music-for-the-modern-monkey
[Music for the Modern Monkey]: https://www.jamendo.com/en/list/a126867/music-for-the-modern-monkey
[*Mystic*]: https://soundcloud.com/crystaldrop/sets/mystic
[Nat King Cole (Accrophone Remix) (Archived)]: https://web.archive.org/web/20150130081224/https://alaclairensemble.bandcamp.com/track/nat-king-cole-accrophone-remix
[Nat King Cole (Accrophone Remix)]: https://alaclairensemble.bandcamp.com/track/nat-king-cole-accrophone-remix
[*Never Set Chickens on Fire!* by __Zerbra Mussel__]: http://www.jamendo.com/album/112114/
[Nikki]: https://soundcloud.com/nicola-dodds-1
[*No Agenda Show*]: http://noagendashow.com  
[No Agenda Show]: http://noagendashow.com
[\@OgdenAlaclair]: https://twitter.com/OgdenAlaclair
[*One Million Dollar Surf Band* by __The Dead Rocks__]: http://deadrocks.bandcamp.com/album/one-million-dollar-surf-band/
[On Returning]: http://www.jamendo.com/en/artist/On_returning
[*On the Shore* by __Ceilí Moss__]: https://www.jamendo.com/en/list/a139261/on­the­shore
[Out of It]: http://www.bradsucks.net/albums/out_of_it/
[*Palingenesi* by __Moreno Visintin__]: http://www.jamendo.com/album/140466/
[Parisian]: http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1100120
[*Pen* (Archived)]: https://web.archive.org/web/20150215225710/https://www.jamendo.com/en/list/a142451/pen
[Pen]: https://www.jamendo.com/en/list/a142451/pen
[*Penumbra*]: http://ofplants.bandcamp.com/album/penumbra
[*Petal* (Archived)]: https://web.archive.org/web/20150215222031/https://brokeforfree.bandcamp.com/album/petal
[Petal]: https://brokeforfree.bandcamp.com/album/petal
[Phaserland]: https://soundcloud.com/phaserlandmusic
[*Practice: Alap 1 - Sadhana in Bhairav* by __Vibhavaree Gargeya__]: http://www.jamendo.com/album/88618/
[Punctum Books]: http://punctumbooks.com
[*Realize* by __CyberSDF__]: https://www.jamendo.com/en/list/a140819/realize
[__Reole__]: http://www.reolemusic.com/
[Reole]: http://www.reolemusic.com
[\@reolemusic]: https://twitter.com/reolemusic
[Retro Promenade]: http://retropromenade.bandcamp.com
[*Retrospectacles* (Archived)]: https://web.archive.org/web/20150215153845/https://www.jamendo.com/en/list/a37596/retrospectacles 
[Retrospectacles (Tulrich.com)]: http://tulrich.com/sinkhole/retrospectacles/index.html
[Retrospectacles]: www.jamendo.com/en/list/a37596/retrospectacles
[\@roberto_diana]: https://twitter.com/roberto_diana  
[Roberto Diana]: http://www.robertodiana.com/
[*Route 9* (Archived)]: https://web.archive.org/web/20150215212300/http://freemusicarchive.org/music/Uncle_Milk/Route_9
[Route 9]: http://freemusicarchive.org/music/Uncle_Milk/Route_9
[*Rue Sicard* (Archived)]: https://web.archive.org/web/20150130081539/https://alaclairensemble.bandcamp.com/album/rue-sicard  
[Rue Sicard]: https://alaclairensemble.bandcamp.com/album/rue-sicard  
[*Run Run Superman* by __Rinzo & The Hey Hey Panthers__]: http://www.jamendo.com/album/140446/  
[S.A. Bach]: http://sabach.bandcamp.com
[*Safe as Milk Replica* (Archived)]: https://web.archive.org/web/20150215230508/https://allover.bandcamp.com/album/safe-as-milk-replica
[Safe as Milk Replica]: https://allover.bandcamp.com/album/safe-as-milk-replica
[Saint Jayne]: http://www.stjayne.com/
[__Sergio__]: http://www.jamendo.com/en/artist/458922
[Shad & DJ T.Lo]: http://www.shadk.com/
[\@shadkmusic]: http://www.twitter.com/shadkmusic
[Silent Tears in a City of Strangers]: http://www.jamendo.com/album/112840/
[Sinkhole]: http://slower.net/sinkhole
[*Slow Names*]: http://www.ofplants.com/catalog/002/  
[*Soul Africa* by __Juanitos__]: http://www.jamendo.com/album/59660/
[__Spiedkiks__]: http://rec72.net/?cat=38  
[Spiedkiks]: http://rec72.net/?cat=38  
[Stuart Ross]: http://www.stuartrossmusic.com/
[SVYNV]: http://thelxstmusic.tumblr.com
[Swahili]: http://www.swahilinoise.com/
[\@swahilinoise]: https://twitter.com/Swahilinoise
[*Take Off Your Makeup* by __Spiedkiks__]: http://rec72.net/?p=3221
[__T Bird & the Breaks__]: http://music.tbirdandthebreaks.com
[*The Beautiful Machine* (Archived)]: https://web.archive.org/web/20150206021458/https://www.jamendo.com/en/list/a135128/the-beautiful-machine
[The Beautiful Machine]: https://www.jamendo.com/en/list/a135128/the-beautiful-machine
[The Fucked Up Beat]: http://thefuckedupbeat.bandcamp.com
[The Ghost Town Memorial Blues Marching Band Anthology: Volume 2]: https://www.jamendo.com/en/list/a141976/the-ghost-town-memorial-blues-marching-band-anthology-vol.2
[The Imaginary Suitcase]: http://www.theimaginarysuitcase.be/
[\@thelxst]: https://twitter.com/thelxst
[*The Magic Empire* by __Uniform Motion__]: http://web.archive.org/web/20130614061531/http://www.jamendo.com/en/list/a122389/the­magic­empire
[The Mind Orchestra]: http://www.themindorchestra.com/  
[*The Scientific Method, Volume II...* by __Professor Kliq__]: https://web.archive.org/web/20090205052259/http://www.jamendo.com/en/album/32636
[*The Simpsons*]: http://www.simpsonsworld.com
[*The White Rhino* by __Julian Rhine__]: https://www.jamendo.com/en/list/a141993/the-white-rhino
[Thieves Like Us]: http://www.jamendo.com/album/68439/
[Think of the Children]: https://www.youtube.com/watch?v=RybNI0KB1bg
[\@thoola]: https://twitter.com/thoola
[Thoola's eponymous album]: https://www.jamendo.com/en/list/a134661/thoola
[Tlushch]: https://vk.com/tlushch
[*Tomate la Shanté* by __MiL__]: http://www.jamendo.com/album/140497/
[*Toute Est Impossible* by __Alaclair Ensemble__]: http://alaclairensemble.bandcamp.com/album/toute­est­impossible
[Uncle Milk]: https://soundcloud.com/unclemilk
[*Vox Populi 2: A Sequel* (Archived)]: https://web.archive.org/web/20150130071734/https://retropromenade.bandcamp.com/album/vox-populi-2-a-sequel
[*Vox Populi 2: A Sequel* by __Retro Promenade__ & Various Artists]: http://retropromenade.bandcamp.com/album/vox-populi-2-a-sequel
[Vox Populi 2: A Sequel]: https://retropromenade.bandcamp.com/album/vox-populi-2-a-sequel
[*Wage War* by __Circa Vitae__]: http://www.jamendo.com/album/119336/
[*Way Back* (Archived)]: https://web.archive.org/web/20150215221548/https://www.jamendo.com/en/list/a122702/way-back
[*Way Back* by __Stuart Ross__]: https://www.jamendo.com/en/list/a122702/way-back
[Way Back]: https://www.jamendo.com/en/list/a122702/way-back
[*What Once Was* (Archived)]: https://web.archive.org/web/20150130074129/https://www.jamendo.com/en/list/a142937/what-once-was
[What Once Was]: https://www.jamendo.com/en/list/a142937/what-once-was
[*What's Later?* by __Apache Tomcat__]: http://freemusicarchive.org/music/Apache_Tomcat/Whats_Later/
[*What's Later*]: http://freemusicarchive.org/music/Apache_Tomcat/Whats_Later/
[*What We Want* by __The EGOTWISTERs__]: http://egotwisters.bandcamp.com/
[*Wheels Must Roll* by __Bugotak__]: https://www.jamendo.com/en/list/a53211/wheels-must-roll
[*Wild Heart* (Archived)]: https://web.archive.org/web/20150130073432/https://www.jamendo.com/en/list/a142876/wild-heart  
[Wild Heart]: https://www.jamendo.com/en/list/a142876/wild-heart
[*Wildhigh EP* (Archived)]: https://web.archive.org/web/20150206020256/https://www.jamendo.com/en/list/a133100/wildhigh-ep
[Wildhigh EP]: https://www.jamendo.com/en/list/a133100/wildhigh-ep
[*Wild Shores* (Archived)]: https://web.archive.org/web/20150215154737/http://freemusicarchive.org/music/Wild_Shores/Wild_Shores
[Wild Shores]: https://mattyz.bandcamp.com/album/wild-shores
[Wild Shores (Sample)]: http://freemusicarchive.org/music/Wild_Shores/Wild_Shores
[WR10]: http://www.jamendo.com/en/artist/WR10
[*Ya Heard Me* (Archived)]: https://web.archive.org/web/20150215213816/https://www.jamendo.com/en/list/a120802/ya-heard-me
[*Ya Heard Me* by __Reole__]: https://www.jamendo.com/en/list/a120802/ya-heard-me
[Ya Heard Me]: https://www.jamendo.com/en/list/a120802/ya-heard-me

[Graveyard Junction]: https://panchromaticrecords.bandcamp.com/track/graveyard-junction
[Sawtooth]: http://facebook.com/sawtoothfolk
[SAW2TH]: http://sawtoothfolk.bandcamp.com/album/saw2th
[Heaven Among the Hills]: http://sawtoothfolk.bandcamp.com/album/heaven-among-the-hills
[Collection]: http://freemusicarchive.org/music/Squire_Tuck/2015021152908025
[Squire Tuck]: http://freemusicarchive.org/music/Squire_Tuck/
[Core Sample]: http://tulrich.com/sinkhole/core_sample/
[dayvision]: http://freemusicarchive.org/music/Epsilon_Not/dayvision/
[epsilon not]: http://freemusicarchive.org/music/Epsilon_Not/
[The Shooters]: https://www.jamendo.com/en/artist/362874/the-shooters
[Rock Pedal to the Floor]: https://www.jamendo.com/en/list/a74766/rock-pedal-to-the-floor
[Just Had To Let You Know]: https://www.jamendo.com/en/list/a97361/just-had-to-let-you-know
[The Scientific Method, Volume II: Experiments in Sound and Perspective]: http://soundcloud.com/professorkliq/sets/the-scientific-method-vol-ii
[Professor Kliq]: http://www.professorkliq.com
[Feeling Good]: http://viennaditto.bandcamp.com/album/feeling-good-opium-boys
[Vienna Ditto]: http://www.viennaditto.com/
[Exile in Blue (Best of II)]: https://www.jamendo.com/en/list/a128591/exile-in-blue-best-of-ii
[Roy Smiles]: http://www.reverbnation.com/roysmiles
[A Bullet by Any Other Name]: http://sunshineirony.bandcamp.com/album/a-bullet-by-any-other-name  
[Sunshine & Irony]: https://www.facebook.com/SunshineandIrony?sk=app_178091127385
[Entrance]: https://avkh.bandcamp.com/album/entrance  
[Andrey Avkhimovich]: http://avkh.org.ru/
[This Riders Song]: https://www.jamendo.com/en/list/a144094/this-riders-song
[Jeffrey Phillip Nelson]: http://www.jeffreyphilipnelson.com/
[Christoph Nolte]: http://www.nolte-sprecher.de/
[The Rocky Road]: https://www.jamendo.com/en/list/a54242/the-rocky-road
[Distimia]: https://www.jamendo.com/en/artist/4163/distimia-espa-a  
[20lb Sounds]: http://20lb.bandcamp.com  
[Shad & Scratch Bastid (Co-produced by Jazzy Jeff)]: http://shadk.bandcamp.com  
[Spinning Clocks]: http://freemusicarchive.org/music/Spinning_Clocks/Spirits_in_the_Juice/Circle_Round  
[nathaxn walker]: http://nathaxnwalker.bandcamp.com  
[No Agenda Episode 697]: http://697.noagendanotes.com  
[Potta]: https://www.jamendo.com/en/artist/459295/potta


<!-- TEXT -->

[CC™ BY 2.0 FR]: http://creativecommons.org/licenses/by/2.0/fr/
[CC™ BY 3.0 FR]: http://creativecommons.org/licenses/by-sa/3.0/fr/
[CC™ BY 3.0]: http://creativecommons.org/licenses/by/3.0/
[CC™ BY 4.0]: http://creativecommons.org/licenses/by/4.0/
[CC™ BY]: http://creativecommons.org/licenses/by/3.0/
[CC™ BY-NC-ND 3.0]: http://creativecommons.org/licenses/by-nc-nd/3.0/
[CC™ BY-NC-ND]: http://creativecommons.org/licenses/by-nc-nd/3.0/  
[CC™ BY-NC-SA 3.0]: http://creativecommons.org/licenses/by-nc-sa/3.0/
[CC™ BY-NC-SA]: http://creativecommons.org/licenses/by-nc-sa/3.0/  
[CC™ BY-SA 2.0 FR]: http://creativecommons.org/licenses/by-sa/2.0/fr/
[CC™ BY-SA 2.0 UK]: http://creativecommons.org/licenses/by-sa/2.0/uk/
[CC™ BY-SA 2.5 IT]: http://creativecommons.org/licenses/by-sa/2.5/it/
[CC™ BY-SA 3.0 DE]: http://creativecommons.org/licenses/by-sa/3.0/de/
[CC™ BY-SA 3.0]: http://creativecommons.org/licenses/by-sa/3.0/
[CC™ BY-SA 4.0]: http://creativecommons.org/licenses/by-sa/4.0/
[CC™ BY-SA]: http://creativecommons.org/licenses/by-sa/3.0/
[CC™ Mixter: A Memoir]: http://fourstones.net/ccmixter-a-memoir
[CC™™ Zero]: http://creativecommons.org/publicdomain/zero/1.0/
[CC™ Zero]: http://creativecommons.org/publicdomain/zero/1.0/
[Poems Every Child Should Know]: http://www.gutenberg.org/1/6/4/3/16436/
[*The Song of Hiawatha*]: http://www.gutenberg.org/ebooks/19
[Victor Stone]: http://fourstones.net/view/media/about


<!-- OTHER -->

[__Aesthesea__]: http://aesthesea.com  
[Alaclair Ensemble]: http://alaclairensemble.bandcamp.com
[Alex O'Brien]: http://amateurzen.us
[*KFUG Community Radio*]: http://kfugradio.org/
[KFUG Community Radio]: http://kfugradio.org/
[*#OO Show*]: http://rynothebearded.com/category/ooshow/
[#OO Show]: http://rynothebearded.com/category/ooshow/
[*#OO Stream*]: https://vicky.glump.net/pintsize/?musicstreamvote_player=1
[#OO Stream]: https://vicky.glump.net/pintsize/?musicstreamvote_player=1
[RynoTheBearded]: https://twitter.com/rynothebearded
[Starfrosch]: http://starfrosch.ch/
[Stefan Molyneux]: http://freedomainradio.com
[Stratford Ct.]: http://stratfordct.bandcamp.com  
[Zeopolis Productions]: https://archive.org/details/zeopolisproductions


<!-- MINE -->

[a contact form]: http://www.amateurzen.us/contactme/
[Amateur Zen]: http://amateurzen.us
[*amateurzen.us*]: http://amateurzen.us
[my *About* page]: http://www.amateurzen.us/about/
[Subscription to the *Amateur Zen 'Cast*]: http://feeds.feedburner.com/a-zen/
[Subscription to the *Amateur Zen 'Casts*]: http://feeds.feedburner.com/a-zen/
[Subscription to the *Libreyana 'Cast*]: http://feeds.feedburner.com/libreyana/

